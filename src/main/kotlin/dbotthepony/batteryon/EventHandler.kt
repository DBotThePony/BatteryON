package dbotthepony.batteryon

import baubles.api.BaublesApi
import baubles.api.cap.IBaublesItemHandler
import dbotthepony.batteryon.BatteryON.Companion.MODID
import dbotthepony.batteryon.BatteryON.Companion.logger
import net.minecraft.client.resources.I18n
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NBTTagCompound
import net.minecraftforge.energy.CapabilityEnergy
import net.minecraftforge.event.entity.living.LivingEvent
import net.minecraftforge.event.entity.player.ItemTooltipEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.Optional;
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import net.minecraft.client.renderer.RenderHelper
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.RenderItem
import net.minecraftforge.fml.client.FMLClientHandler
import net.minecraftforge.client.event.RenderTooltipEvent
import net.minecraftforge.fml.common.eventhandler.EventPriority






// 
// Copyright (C) 2018 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

fun formatNumber(num: Int): String {
	if (num < 0) {
		return num.toString()
	} else if (num < 1000) {
		return num.toString()
	} else if (num < 1000000) {
		return (num / 1000F).toString() + "k"
	} else if (num < 1000000000) {
		return (num / 1000000F).toString() + "M"
	} else if (num < 1000000000000) {
		return (num / 1000000000.0).toString() + "B"
	}

	return "0"
}

fun updateItem(item: ItemStack) {
	if (item.isEmpty) {
		return
	}

	if (item.tagCompound == null) {
		return
	}

	if (!item.tagCompound!!.hasKey(MODID)) {
		return
	}

	val nbt = item.tagCompound!!.getTag(MODID) as NBTTagCompound
	val battery = ItemStack(nbt)

	if (!battery.hasCapability(CapabilityEnergy.ENERGY, null) || !item.hasCapability(CapabilityEnergy.ENERGY, null)) {
		return
	}

	val batteryEnergy = battery.getCapability(CapabilityEnergy.ENERGY, null) ?: return
	val itemEnergy = item.getCapability(CapabilityEnergy.ENERGY, null) ?: return

	if (!itemEnergy.canReceive() || !batteryEnergy.canExtract()) {
		return
	}

	val missing = itemEnergy.maxEnergyStored - itemEnergy.energyStored
	val receive = itemEnergy.receiveEnergy(missing, true)
	val extracted = batteryEnergy.extractEnergy(receive, true)
	val transfer = Math.min(receive, extracted)
	itemEnergy.receiveEnergy(transfer, false)
	batteryEnergy.extractEnergy(transfer, false)
}

class BatteryEventHandler {
	@SubscribeEvent
	fun onLivingUpdate(event: LivingEvent.LivingUpdateEvent) {
		val ent = event.entityLiving

		if (!(ent is EntityPlayer)) {
			return
		}

		if (ent.world.isRemote) {
			return
		}

		// update main items

		for (item in ent.inventory.mainInventory) {
			updateItem(item)
		}

		for (item in ent.inventory.armorInventory) {
			updateItem(item)
		}

		for (item in ent.inventory.offHandInventory) {
			updateItem(item)
		}
	}

	@SubscribeEvent
	@Optional.Method(modid = "baubles")
	fun onLivingUpdateBaubles(event: LivingEvent.LivingUpdateEvent) {
		val ent = event.entityLiving as? EntityPlayer ?: return

		if (ent.world.isRemote) {
			return
		}

		val baubles: IBaublesItemHandler = BaublesApi.getBaublesHandler(ent) ?: return

		for (i in 0 until baubles.slots) {
			updateItem(baubles.getStackInSlot(i))
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.LOWEST)
	fun updateDescription(event: ItemTooltipEvent) {
		val stack = event.itemStack

		if (stack.isEmpty || !stack.hasTagCompound() || !stack.tagCompound!!.hasKey(BatteryON.MODID)) {
			return
		}

		val nbt = stack.tagCompound!!.getTag(MODID) as NBTTagCompound
		val battery = ItemStack(nbt)

		if (battery.isEmpty || !battery.hasCapability(CapabilityEnergy.ENERGY, null)) {
			return
		}

		val batteryEnergy = battery.getCapability(CapabilityEnergy.ENERGY, null) ?: return

		val stored = batteryEnergy.energyStored
		val max = batteryEnergy.maxEnergyStored

		event.toolTip.add("")
		event.toolTip.add("     " + I18n.format("info.batteryon.attachinfo", battery.displayName))
		event.toolTip.add("     " + I18n.format("info.batteryon.energyinfo", formatNumber(stored), formatNumber(max)))
	}

	private var itemRenderer: RenderItem? = null

	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.LOWEST)
	fun tooltipRenderPost(event: RenderTooltipEvent.PostText) {
		if (itemRenderer == null) {
			itemRenderer = FMLClientHandler.instance().client.renderItem
		}

		val stack = event.stack

		if (stack.isEmpty || !stack.hasTagCompound() || !stack.tagCompound!!.hasKey(MODID)) {
			return
		}

		val tag = stack.tagCompound!!.getTag(MODID) as NBTTagCompound
		val battery = ItemStack(tag)

		if (battery.isEmpty || !battery.hasCapability(CapabilityEnergy.ENERGY, null)) {
			return
		}

		val x = event.x
		val y = event.y + event.height - 16

		GlStateManager.pushMatrix()
		RenderHelper.enableGUIStandardItemLighting()
		GlStateManager.disableLighting()
		GlStateManager.enableRescaleNormal()
		GlStateManager.enableColorMaterial()
		GlStateManager.enableLighting()

		try {
			itemRenderer!!.zLevel = 100.0f
			itemRenderer!!.renderItemAndEffectIntoGUI(battery, x, y)
			itemRenderer!!.renderItemOverlays(event.fontRenderer, battery, x, y)
			itemRenderer!!.zLevel = 0.0f
		} catch(err: Throwable) {
			logger?.error(err)
		}

		GlStateManager.popMatrix()
		GlStateManager.enableLighting()
		GlStateManager.enableDepth()
		RenderHelper.enableStandardItemLighting()
	}
}
