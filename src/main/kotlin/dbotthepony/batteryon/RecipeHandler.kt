package dbotthepony.batteryon

// 
// Copyright (C) 2018 DBot
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
//  
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import net.minecraft.inventory.InventoryCrafting
import net.minecraft.item.ItemBlock
import net.minecraft.item.ItemStack
import net.minecraft.item.crafting.IRecipe
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.util.NonNullList
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraftforge.common.ForgeHooks
import net.minecraftforge.energy.CapabilityEnergy

fun itemAmount(inv: InventoryCrafting): Int {
	var stacks = 0

	for (i in 0 until inv.sizeInventory) {
		if (!inv.getStackInSlot(i).isEmpty) {
			stacks++
		}
	}

	return stacks
}

fun firstItem(inv: InventoryCrafting): ItemStack {
	for (i in 0 until inv.sizeInventory) {
		if (!inv.getStackInSlot(i).isEmpty) {
			return inv.getStackInSlot(i)
		}
	}

	return ItemStack.EMPTY
}

class RecipeHandler : net.minecraftforge.registries.IForgeRegistryEntry.Impl<IRecipe>, IRecipe {
	constructor() : super() {
		this.registryName = ResourceLocation(BatteryON.MODID, "battery_attach")
	}

	override fun canFit(width: Int, height: Int): Boolean {
		return width >= 2 || height >= 2
	}

	override fun getRecipeOutput(): ItemStack {
		return ItemStack.EMPTY
	}

	override fun getCraftingResult(inv: InventoryCrafting?): ItemStack {
		val items = itemAmount(inv!!)

		if (items == 2) {
			val target: ItemStack = inv.getStackInSlot(0)
			val battery: ItemStack = inv.getStackInSlot(1)

			if (target.isEmpty || battery.isEmpty || !target.hasCapability(CapabilityEnergy.ENERGY, null) || !battery.hasCapability(CapabilityEnergy.ENERGY, null) || target.item is ItemBlock) {
				return ItemStack.EMPTY
			}

			if (!target.getCapability(CapabilityEnergy.ENERGY, null)!!.canReceive()) {
				return ItemStack.EMPTY
			}

			if (!battery.getCapability(CapabilityEnergy.ENERGY, null)!!.canExtract()) {
				return ItemStack.EMPTY
			}

			if (target.hasTagCompound() && target.tagCompound!!.hasKey(BatteryON.MODID)) {
				val stack = ItemStack(target.tagCompound!!.getTag(BatteryON.MODID) as NBTTagCompound)

				if (!stack.isEmpty) {
					return ItemStack.EMPTY
				}
			}

			val output = target.copy()
			val serialize = battery.serializeNBT()

			if (!output.hasTagCompound()) {
				output.tagCompound = NBTTagCompound()
			}

			output.tagCompound!!.setTag(BatteryON.MODID, serialize)
			return output
		} else if (items == 1) {
			val item = firstItem(inv)

			if (item.isEmpty || !item.hasTagCompound() || !item.tagCompound!!.hasKey(BatteryON.MODID)) {
				return ItemStack.EMPTY
			}

			val copy = item.copy()
			copy.tagCompound!!.removeTag(BatteryON.MODID)
			return copy
		}

		return ItemStack.EMPTY
	}

	override fun matches(inv: InventoryCrafting?, worldIn: World?): Boolean {
		val items = itemAmount(inv!!)

		if (items == 2) {
			val target: ItemStack = inv.getStackInSlot(0)
			val battery: ItemStack = inv.getStackInSlot(1)
			return target.hasCapability(CapabilityEnergy.ENERGY, null) &&
					battery.hasCapability(CapabilityEnergy.ENERGY, null) &&
					!(target.item is ItemBlock) &&
					target.getCapability(CapabilityEnergy.ENERGY, null)!!.canReceive() &&
					battery.getCapability(CapabilityEnergy.ENERGY, null)!!.canExtract()
		} else if (items == 1) {
			val item = firstItem(inv)

			return item.hasTagCompound() &&
					item.tagCompound!!.hasKey(BatteryON.MODID)
		} else {
			return false
		}
	}

	override fun getRemainingItems(inv: InventoryCrafting): NonNullList<ItemStack> {
		val remaining = ForgeHooks.defaultRecipeGetRemainingItems(inv)

		if (itemAmount(inv) == 1) {
			for (i in 0 until inv.sizeInventory) {
				val item = inv.getStackInSlot(i)

				if (!item.isEmpty) {
					remaining[i] = ItemStack(item.tagCompound!!.getTag(BatteryON.MODID) as NBTTagCompound)
				}
			}
		}

		return remaining
	}
}
